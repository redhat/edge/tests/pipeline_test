RHIVOS Toolchain Pipeline end to end (smoke) test
-------------------------------------------------

This test can be run locally. Command's that are needed:
sed - system package
oc - https://docs.openshift.com/container-platform/4.7/cli_reference/openshift_cli/getting-started-cli.html
tkn - https://docs.openshift.com/container-platform/4.7/cli_reference/tkn_cli/installing-tkn.html
git - system package
amqpcli - pip install amqp-client-cli

The following environment variables can be set, or check.sh modified directly.

$RHIVOS_TOOLCHAIN_PIPELINE_E2E_URL_A
$RHIVOS_TOOLCHAIN_PIPELINE_E2E_UN_A
$RHIVOS_TOOLCHAIN_PIPELINE_E2E_PW_A
$RHIVOS_TOOLCHAIN_PIPELINE_E2E_URL_B
$RHIVOS_TOOLCHAIN_PIPELINE_E2E_UN_B
$RHIVOS_TOOLCHAIN_PIPELINE_E2E_PW_B

The "A" variables are for the microservices cluster, and the "B" variables are
for the pipeline cluster. The default URLs are set in check.sh. However the
credentials are not.

TODO: Listen for merge request ID on message queue.
TODO: Use tmt to execute in a container.
