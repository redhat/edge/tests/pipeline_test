#!/bin/sh
DEBUG=0
CMD_OC="oc"
CMD_TKN="tkn"
CMD_GIT="git"
CMD_SED="sed"
CMD_AMQPCLI="amqpcli"
GIT_URL="git@gitlab.com:redhat/edge/ci-cd/manifests.git"
OS_URL_A="https://api.auto-prod.gi0n.p1.openshiftapps.com:6443"
OS_URL_B="https://api.test-cluster.pxso.p1.openshiftapps.com:6443"
OS_UN_A="can be overwritten from env"
# OS_PW_X=if not set, OS_UN_X should be a token
OS_UN_B="can be overwritten from env"
OS_PROJECT_A="toolchain"
OS_PROJECT_B="main"
OS_BUSPORT="5672"
CHECK_COUNT="$(seq 1 10)"
POD="rabbit-mq"
MRID=""

if [ $DEBUG -eq 1 ]; then set -x; fi

check_env()
{
    # Check we have the required commands
    check_cmd "$CMD_OC"
    check_cmd "$CMD_TKN"
    check_cmd "$CMD_GIT"
    check_cmd "$CMD_SED"
    check_cmd "$CMD_AMQPCLI"

    if [ -n "$RHIVOS_TOOLCHAIN_PIPELINE_E2E_URL_A" ]; then
        OS_URL_A=$RHIVOS_TOOLCHAIN_PIPELINE_E2E_URL_A
    fi
    if [ -n "$RHIVOS_TOOLCHAIN_PIPELINE_E2E_UN_A" ]; then
        OS_UN_A=$RHIVOS_TOOLCHAIN_PIPELINE_E2E_UN_A
    fi
    if [ -n "$RHIVOS_TOOLCHAIN_PIPELINE_E2E_PW_A" ]; then
        OS_PW_A=$RHIVOS_TOOLCHAIN_PIPELINE_E2E_PW_A
    fi
    if [ -n "$RHIVOS_TOOLCHAIN_PIPELINE_E2E_URL_B" ]; then
        OS_URL_B=$RHIVOS_TOOLCHAIN_PIPELINE_E2E_URL_B
    fi
    if [ -n "$RHIVOS_TOOLCHAIN_PIPELINE_E2E_UN_B" ]; then
        OS_UN_B=$RHIVOS_TOOLCHAIN_PIPELINE_E2E_UN_B
    fi
    if [ -n "$RHIVOS_TOOLCHAIN_PIPELINE_E2E_PW_B" ]; then
        OS_PW_B=$RHIVOS_TOOLCHAIN_PIPELINE_E2E_PW_B
    fi
}

check_cmd()
{
    local CMD=$1
    local PATH="$(command -v "$CMD")"
    if [ -z "$PATH" ]; then
        echo "$CMD not found. Aborting."
        exit 1
    fi
    if [ $DEBUG -eq 1 ]; then echo "Using $PATH"; fi
}

os_login()
{
    local URL=$1
    local PROJECT=$2
    # Logout if we are already logged in.
    $CMD_OC whoami &> /dev/null
    if [ $? -eq 0 ]; then
        echo "logging out..."
        os_logout
    fi

    # Login to the OpenShift cluster
    if [ -z $4 ]; then
        # Only 2 parameters provided. Hence, the 2nd parameter is a token.
        $CMD_OC login $URL --token $3
    else
        $CMD_OC login $URL --username $3 --password $4
    fi
    if [ $? -ne 0 ]; then
        echo "Could not login to OpenShift cluster."
        exit 1
    fi

    # Change to the correct project
    $CMD_OC project $PROJECT
    if [ $? -ne 0 ]; then
        echo "Could not find project: $PROJECT"
        os_logout
        exit 1
    fi
}

push_mr()
{
    echo "Starting pipeline by making a change to gitlab repo..."
    echo "Cloning $GIT_URL in $(pwd)"
    $CMD_GIT clone $GIT_URL
    if [ $? -ne 0 ]; then
        echo "Could not clone repo: $GIT_URL"
        return 1
    fi
    DIR="$(echo "$GIT_URL" | $CMD_SED -nr "s/^.+\/(.+)\.git$/\1/p")"
    cd "$DIR"
    if [ $? -ne 0 ]; then
        echo "There is something wrong with the repository."
        return 1
    fi
    local DATE="$(date --rfc-3339=ns)"
    local BRANCH="e2e_$(date +%s)"
    local DESC="Draft: Automated e2e test on $DATE - do not merge!"
    $CMD_GIT checkout -b "$BRANCH"
    tail -n +2 package_list/c8s-image-manifest.txt > package_list/c8s-image-manifest.txt
    if [ $DEBUG -eq 1 ]; then $CMD_GIT status; fi
    exit 1
    $CMD_GIT add -A
    $CMD_GIT commit -m "$DESC"
    local R="$($CMD_GIT push --porcelain \
                             -o merge_request.create \
                             -o merge_request.target=main \
                             -o merge_request.description="$DESC" \
                             origin "$BRANCH" 2>&1)"
    if [ $DEBUG -eq 1 ]; then echo "$R"; fi
    CHECK="$(echo "$R" | $CMD_SED -nr "s/^(remote: View merge request for $BRANCH:).*$/\1/p")"
    if [ -z "$CHECK" ]; then
        echo "Could not find merge request ID!"
        return 1
    fi
    MRID="$(echo "$R" | $CMD_SED -nr "s/^remote:\s+https\S+merge_requests\/(\S+)\s*$/\1/p")"
    if [ -z "$CHECK" ]; then
        echo "Could not find merge request ID!"
        return 1
    fi
    cd ..
    rm $DIR -Rf
}

send_msg()
{
    echo "Starting pipeline by publishing message to queue..."
    $CMD_OC port-forward deployment/$POD $OS_BUSPORT:$OS_BUSPORT &
    PID=$!
    sleep 5
    $CMD_AMQPCLI send localhost 5672 amq.topic c8s_build -m "test payload - use -f" -u guest
    # not sure if I can get merge request ID from the bus
    MRID=""
    kill $PID
}

check_pipeline()
{
    if [ -z $MRID ]; then
        echo "No merge request ID (\$MRID) set!"
        return 1
    fi
    echo "Checking for pipelinerun for $MRID..."
    for TRY in $CHECK_COUNT; do
        local LIST="$($CMD_TKN pipelinerun list --no-headers --no-color)"
        RUNS="$(echo "$LIST" | $CMD_SED -nr "s/^(manifests-${MRID}-\S+)\s.*$/\1/p")"
        if [ -n "$RUNS" ]; then
            for RUN in $RUNS; do
                echo "Checking: $RUN"
                $CMD_TKN pipelinerun describe $RUN
            done
            return 0
        fi
        sleep 10
    done
    echo "Could not find pipelinerun for $MRID!"
    return 1
}

os_logout()
{
    # Logout of the OpenShift cluster
    $CMD_OC logout
    if [ $? -ne 0 ]; then
        echo "Could not logout."
        exit 1
    fi
}

check_env
os_login $OS_URL_A $OS_PROJECT_A $OS_UN_A $OS_PW_A
send_msg
os_logout
if [ -z $MRID ]; then
    echo "Could not get merge request ID from message queue. Trying merge request..."
    push_mr
    if [ $? -ne 0 ]; then
        echo "Could not make merge request."
        exit 1
    fi
fi
os_login $OS_URL_B $OS_PROJECT_B $OS_UN_B $OS_PW_B
check_pipeline
os_logout

if [ $DEBUG -eq 1 ]; then set +x; fi
